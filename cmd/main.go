package main

import (
	"log"
	"strings"

	"bitbucket.org/ccat/lambda_cms/pkg"
	//"bitbucket.org/ccat/tinylib"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

func topEvent(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	config, err := pkg.GetCmsConfig()
	if err != nil { //&& config.Status != "production" {
		log.Printf("MAIN: Failed to generate CmsConfig: %v", err)
		res := events.APIGatewayProxyResponse{}
		res.Headers = map[string]string{"Content-type": "text/html"}
		res.StatusCode = 500
		res.IsBase64Encoded = false
		res.Body = "FATAL ERROR"
		return res, err
	}

	param, user := pkg.GenerateParam(request)
	param["URL_PREFIX"] = config.GetURL_PREFIX()
	param["COOKIE_PATH"] = "/"

	path := request.Path
	if strings.HasSuffix(path, "/") {
		path = path[:len(path)-1]
	}
	pathList := strings.Split(path, "/")
	lastPath := pathList[len(pathList)-1]

	log.Printf("%v,%v", request.Path, user.GetSort())

	//param["template_s3_bucket"] = os.Getenv("TEMPLATE_S3_BUCKET") //us-east-1, ap-northeast-1,...
	/*template_s3_region := os.Getenv("TEMPLATE_S3_REGION") //us-east-1, ap-northeast-1,...
	template_s3_bucket := os.Getenv("TEMPLATE_S3_BUCKET")
	if template_s3_bucket != "" {
		_, err := os.Stat("/tmp/template")
		if os.IsNotExist(err) {
			res := events.APIGatewayProxyResponse{}
			message := fmt.Sprintf("IsNotExist")
			res.StatusCode = 500
			res.Body = message
			return res, errors.New(message)
		}
	}*/

	if config.GetStatusCode() <= 10 {
		if lastPath == "config" {
			return pkg.ShowConfig(request, param, user)
		}
		return pkg.Redirect("./config/", param), nil
	}
	if strings.HasPrefix(request.Path, config.GetURL_PREFIX()+"/config/") {
		return pkg.ShowConfig(request, param, user)
	} else if strings.HasPrefix(request.Path, config.GetURL_PREFIX()+"/account/") {
		return pkg.ShowAccountPage(config.GetURL_PREFIX()+"/account", request, param, user)
	} else if strings.HasPrefix(request.Path, config.GetURL_PREFIX()+"/termofuse/") {
		return pkg.ShowTermOfUsePage(config.GetURL_PREFIX()+"/termofuse", request, param, user)
	}

	return pkg.ShowTemplate(request, "/tmp/template/top.html", param)
}

func main() {
	lambda.Start(topEvent)
}
