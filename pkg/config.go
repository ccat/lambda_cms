package pkg

import (
	"os"
	"strings"

	"bitbucket.org/ccat/tinylib"
	//"github.com/aws/aws-sdk-go/aws"
	//"github.com/aws/aws-sdk-go/aws/session"
	"github.com/ccat/copy"
	"github.com/guregu/dynamo"
	"github.com/pkg/errors"
)

type CmsConfigInterface interface {
	Save() error
	GetStatus() string
	GetStatusCode() int
	GetURL_PREFIX() string
	GetTinyConfig() tinylib.ConfigInterface
	GetTermOfUse() TermOfUseInterface
	GetGoogleOauthClient() string
	GetGoogleOauthSecret() string
	GetTwitterConsumerKey() string
	GetTwitterConsumerSecret() string
	UpdateStatus(string)
	UpdateStatusCode(int)

	setTermOfUse(TermOfUseInterface)
}

type cmsConfig1 struct {
	Prime      string `dynamo:"prime"` //"CONFIG"
	Sort       string `dynamo:"sort"`  //CONFIG
	Status     string
	StatusCode int
	Json       string
	URL_PREFIX string                  `dynamo:"-"`
	TinyConfig tinylib.ConfigInterface `dynamo:"-"`
}

type cmsConfig2 struct {
	Prime                   string `dynamo:"prime"` //"CONFIG"
	Sort                    string `dynamo:"sort"`  //CONFIG
	Status                  string
	StatusCode              int
	Json                    string
	GOOGLE_OAUTH_CLIENT     string
	GOOGLE_OAUTH_SECRET     string
	TWITTER_CONSUMER_KEY    string
	TWITTER_CONSUMER_SECRET string
	URL_PREFIX              string                  `dynamo:"-"`
	TinyConfig              tinylib.ConfigInterface `dynamo:"-"`
	TermOfUse               TermOfUseInterface      `dynamo:"-"`
}

type cmsConfig3 struct {
	Prime                   string `dynamo:"prime"` //"CONFIG"
	Sort                    string `dynamo:"sort"`  //CONFIG
	Status                  string
	StatusCode              int
	Json                    string
	GOOGLE_OAUTH_CLIENT     string
	GOOGLE_OAUTH_SECRET     string
	TWITTER_CONSUMER_KEY    string
	TWITTER_CONSUMER_SECRET string
	URL_PREFIX              string `dynamo:"-"`
	TermOfUseAcceptRequired bool
	TinyConfig              tinylib.ConfigInterface `dynamo:"-"`
	TermOfUse               TermOfUseInterface      `dynamo:"-"`
}

var (
	ErrorConfigNotExists = errors.New("Config does not existed.")
)

var config CmsConfigInterface

func (self *cmsConfig2) Save() error {
	return self.GetTinyConfig().GetDynamoDBtable().Put(self).Run()
}

func (self *cmsConfig2) GetStatus() string {
	return self.Status
}

func (self *cmsConfig2) GetStatusCode() int {
	return self.StatusCode
}

func (self *cmsConfig2) GetURL_PREFIX() string {
	return self.URL_PREFIX
}

func (self *cmsConfig2) GetGoogleOauthClient() string {
	return self.GOOGLE_OAUTH_CLIENT
}

func (self *cmsConfig2) GetGoogleOauthSecret() string {
	return self.GOOGLE_OAUTH_SECRET
}

func (self *cmsConfig2) GetTwitterConsumerKey() string {
	return self.TWITTER_CONSUMER_KEY
}

func (self *cmsConfig2) GetTwitterConsumerSecret() string {
	return self.TWITTER_CONSUMER_SECRET
}

func (self *cmsConfig2) GetTinyConfig() tinylib.ConfigInterface {
	return self.TinyConfig
}

func (self *cmsConfig2) GetTermOfUse() TermOfUseInterface {
	return self.TermOfUse
}

func (self *cmsConfig2) setTermOfUse(term TermOfUseInterface) {
	self.TermOfUse = term
}

func (self *cmsConfig2) UpdateStatus(newStatus string) {
	self.Status = newStatus
}

func (self *cmsConfig2) UpdateStatusCode(newCode int) {
	self.StatusCode = newCode
}

func (self *cmsConfig3) Save() error {
	return self.GetTinyConfig().GetDynamoDBtable().Put(self).Run()
}

func (self *cmsConfig3) GetStatus() string {
	return self.Status
}

func (self *cmsConfig3) GetStatusCode() int {
	return self.StatusCode
}

func (self *cmsConfig3) GetURL_PREFIX() string {
	return self.URL_PREFIX
}

func (self *cmsConfig3) GetGoogleOauthClient() string {
	return self.GOOGLE_OAUTH_CLIENT
}

func (self *cmsConfig3) GetGoogleOauthSecret() string {
	return self.GOOGLE_OAUTH_SECRET
}

func (self *cmsConfig3) GetTwitterConsumerKey() string {
	return self.TWITTER_CONSUMER_KEY
}

func (self *cmsConfig3) GetTwitterConsumerSecret() string {
	return self.TWITTER_CONSUMER_SECRET
}

func (self *cmsConfig3) GetTinyConfig() tinylib.ConfigInterface {
	return self.TinyConfig
}

func (self *cmsConfig3) GetTermOfUse() TermOfUseInterface {
	return self.TermOfUse
}

func (self *cmsConfig3) setTermOfUse(term TermOfUseInterface) {
	self.TermOfUse = term
}

func (self *cmsConfig3) UpdateStatus(newStatus string) {
	self.Status = newStatus
}

func (self *cmsConfig3) UpdateStatusCode(newCode int) {
	self.StatusCode = newCode
}

func GetCmsConfig() (CmsConfigInterface, error) {
	if config == nil {
		err := InitCmsConfig()
		if err != nil {
			return nil, err
		}
	}
	return config, nil
}

func InitCmsConfig() error {
	if config != nil {
		return nil
	}
	tConf, err := tinylib.GetConfig()
	if err != nil {
		return err
	}
	uPrefix := os.Getenv("URL_PREFIX")
	if len(uPrefix) > 0 {
		if strings.HasSuffix(uPrefix, "/") {
			uPrefix = uPrefix[:len(uPrefix)-1]
		}
		if strings.HasPrefix(uPrefix, "/") == false {
			uPrefix = "/" + uPrefix
		}
	}

	configI, err := loadCmsConfig3(tConf, uPrefix)
	saveFlag := false
	if err != nil {
		saveFlag = true
		configI, err = loadCmsConfig2AndConvert3(tConf, uPrefix)
		if err != nil {
			configI, err = loadCmsConfig1AndConvert3(tConf, uPrefix)
			if err != nil {
				createCmsConfig3(tConf, uPrefix)
			}
		}
	}
	configI.TinyConfig = tConf
	if saveFlag {
		err = configI.Save()
	}

	if err != nil {
		return err
	}

	configI.URL_PREFIX = uPrefix

	gClient := os.Getenv("GOOGLE_OAUTH_CLIENT")
	if gClient != "" {
		configI.GOOGLE_OAUTH_CLIENT = gClient
	}
	gSecret := os.Getenv("GOOGLE_OAUTH_SECRET")
	if gSecret != "" {
		configI.GOOGLE_OAUTH_SECRET = gSecret
	}
	tKey := os.Getenv("TWITTER_CONSUMER_KEY")
	if tKey != "" {
		configI.TWITTER_CONSUMER_KEY = tKey
	}
	tSecret := os.Getenv("TWITTER_CONSUMER_SECRET")
	if tSecret != "" {
		configI.TWITTER_CONSUMER_SECRET = tSecret
	}

	_, err = os.Stat("/tmp/template")
	if err != nil {
		basePath := os.Getenv("TEMPLATE_PATH")
		if basePath == "" {
			basePath = "."
		}
		err = copy.Copy(basePath+"/web/default_template", "/tmp/template")
		if err != nil {
			return errors.Wrap(err, "lambda_cms.InitCmsConfig(): failed to copy")
		}
	}

	config = configI
	configI.TermOfUse, _ = GetLatestTermOfUse()
	return err

}

func loadCmsConfig3(tConf tinylib.ConfigInterface, uPrefix string) (*cmsConfig3, error) {
	//table := tConf.GetDynamoDBtable()
	result2 := []cmsConfig3{}
	//err := table.Get("prime", tConf.GetDynamoDBprefix()+"_CONFIG").Range("sort", dynamo.Equal, "CONFIG").All(&result2)
	err := tConf.DynamoTableGet("CONFIG").Range("sort", dynamo.Equal, "CONFIG").All(&result2)
	if err != nil {
		return nil, err
	} else if len(result2) == 0 {
		return nil, ErrorConfigNotExists
	}
	return &result2[0], nil
}

func loadCmsConfig2AndConvert3(tConf tinylib.ConfigInterface, uPrefix string) (*cmsConfig3, error) {
	configI := &cmsConfig3{}

	//table := tConf.GetDynamoDBtable()

	result2 := []cmsConfig2{}
	//err := table.Get("prime", tConf.GetDynamoDBprefix()+"_CONFIG").Range("sort", dynamo.Equal, "CONFIG").All(&result2)
	err := tConf.DynamoTableGet("CONFIG").Range("sort", dynamo.Equal, "CONFIG").All(&result2)
	if err != nil {
		return nil, err
	} else if len(result2) == 0 {
		return nil, ErrorConfigNotExists
	}
	configI.Prime = result2[0].Prime
	configI.Sort = "CONFIG"
	configI.Status = result2[0].Status
	configI.StatusCode = result2[0].StatusCode
	return configI, nil
}

func loadCmsConfig1AndConvert3(tConf tinylib.ConfigInterface, uPrefix string) (*cmsConfig3, error) {
	configI := &cmsConfig3{}

	//table := tConf.GetDynamoDBtable()
	result2 := []cmsConfig1{}
	//err := table.Get("prime", tConf.GetDynamoDBprefix()+"_CONFIG").Range("sort", dynamo.Equal, "CONFIG").All(&result2)
	err := tConf.DynamoTableGet("CONFIG").Range("sort", dynamo.Equal, "CONFIG").All(&result2)
	if err != nil {
		return nil, err
	} else if len(result2) == 0 {
		return nil, ErrorConfigNotExists
	}
	configI.Prime = result2[0].Prime
	configI.Sort = "CONFIG"
	configI.Status = result2[0].Status
	configI.StatusCode = result2[0].StatusCode
	return configI, nil
}

func createCmsConfig3(tConf tinylib.ConfigInterface, uPrefix string) (*cmsConfig3, error) {
	configI := &cmsConfig3{}

	configI.Prime = tConf.GenerateDynamoDBPrime("CONFIG")
	configI.Sort = "CONFIG"
	configI.Status = "prev_create_root"
	configI.StatusCode = 2

	return configI, nil
}
