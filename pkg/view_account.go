package pkg

import (
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"net/url"
	"strings"
	"time"

	"bitbucket.org/ccat/tinylib"
	"github.com/ChimeraCoder/anaconda"
	"github.com/aws/aws-lambda-go/events"
	jwt "github.com/dgrijalva/jwt-go"
	"github.com/garyburd/go-oauth/oauth"
	"github.com/pkg/errors"
	"golang.org/x/oauth2"
	v2 "google.golang.org/api/oauth2/v2"
)

type Oauth2Config struct {
	Config oauth2.Config
}

type Oauth2Token struct {
	Token       string
	ExpiredDate time.Time
	jwt.StandardClaims
}

const (
	twitterRefreshTokenURL  = "https://api.twitter.com/oauth/request_token"
	twitterAuthorizationURL = "https://api.twitter.com/oauth/authenticate"
	twitterAccessTokenURL   = "https://api.twitter.com/oauth/access_token"
	twitterAccountURL       = "https://api.twitter.com/1.1/account/verify_credentials.json"
)

func ShowAccountPage(url_prefix string, request events.APIGatewayProxyRequest, param map[string]interface{}, user tinylib.UserInterface) (events.APIGatewayProxyResponse, error) {
	if config.GetGoogleOauthClient() != "" {
		param["google_login"] = "True"
	} else {
		param["google_login"] = "False"
	}
	if request.Path == url_prefix+"/login/google/" {
		param["redirect"] = request.QueryStringParameters["redirect"]
		return account_google_login(url_prefix, request, param)
	} else if strings.HasPrefix(request.Path, url_prefix+"/login/google/callback/") {
		return account_google_login_callback(url_prefix, request, param)
	} else if request.Path == url_prefix+"/login/twitter/" {
		param["redirect"] = request.QueryStringParameters["redirect"]
		return LoginByTwitter(url_prefix, request, param, user)
	} else if strings.HasPrefix(request.Path, url_prefix+"/login/twitter/callback/") {
		return LoginByTwitterCallback(request, param, user)
	} else if strings.HasPrefix(request.Path, url_prefix+"/login/") {
		if request.HTTPMethod == "GET" {
			param["redirect"] = request.QueryStringParameters["redirect"]
			return showLoginPageTemplate(url_prefix, request, param, user)
		} else if request.HTTPMethod == "POST" {
			return loginByLocalAccount(url_prefix, request, param, user)
		}
	} else if strings.HasPrefix(request.Path, url_prefix+"/logout/") {
		return logout(url_prefix, request, param, user)
	}
	return ShowTemplate(request, "/tmp/template/top.html", param)
}

func showAccount(request events.APIGatewayProxyRequest, param map[string]interface{}, user tinylib.UserInterface) (events.APIGatewayProxyResponse, error) {
	csrf, _ := GenerateCSRF(user, request.Path, 60*5)
	param["CSRF"] = csrf
	if user == nil {
		return Redirect(config.GetURL_PREFIX()+"/account/login/?redirect="+config.GetURL_PREFIX()+"/account/", param), nil
	}
	if request.HTTPMethod == "GET" {
		return ShowTemplate(request, "/tmp/template/account_config.html", param)
	}
	return ShowTemplate(request, "/tmp/template/top.html", nil)
}

func ShowLocalUserCreatePage(url_prefix string, request events.APIGatewayProxyRequest, param map[string]interface{}, user tinylib.UserInterface) (events.APIGatewayProxyResponse, error) {
	csrf, _ := GenerateCSRF(user, request.Path, 60*5)
	param["CSRF"] = csrf
	if request.HTTPMethod == "GET" {
		return ShowTemplate(request, "/tmp/template/account_create.html", param)
	} else if request.HTTPMethod == "POST" {
		postData, err := ParseFormUrlencoded(request)
		if err != nil {
			log.Printf("Invalid POST data. %v", err)
			param["ERROR"] = err
			return ShowLocalUserCreatePage(url_prefix, request, param, user)
		}
		flag, err := ValidateCSRF(postData["CSRF"], user, request.Path)
		if err != nil {
			param["ERROR"] = "CSRF error"
			return ShowLocalUserCreatePage(url_prefix, request, param, user)
		}
		if flag == false {
			param["ERROR"] = "CSRF error"
			return ShowLocalUserCreatePage(url_prefix, request, param, user)
		}
		user, err = tinylib.CreateLocalUser(postData["username"], postData["password"], postData["nickname"])
		if err != nil {
			param["ERROR"] = fmt.Sprintf("Invalid Account.")
			return ShowLocalUserCreatePage(url_prefix, request, param, user)
		}
		session, err := GenerateSession(user)
		if err != nil {
			param["ERROR"] = "Session ERROR"
			return showLoginPageTemplate(url_prefix, request, param, user)
		}
		param = AddCookie2Param(param, "sessions", session, true)
	}
	return Redirect("/", param), nil
}

func loginByLocalAccount(url_prefix string, request events.APIGatewayProxyRequest, param map[string]interface{}, user tinylib.UserInterface) (events.APIGatewayProxyResponse, error) {
	csrf, _ := GenerateCSRF(user, request.Path, 60*5)
	param["CSRF"] = csrf
	postData, err := ParseFormUrlencoded(request)
	if err != nil {
		log.Printf("Invalid POST data. %v", err)
		param["ERROR"] = err
		return showLoginPageTemplate(url_prefix, request, param, user)
	}
	param["redirect"] = postData["redirect"]
	flag, err := ValidateCSRF(postData["CSRF"], user, request.Path)
	if err != nil {
		param["ERROR"] = "CSRF error"
		return showLoginPageTemplate(url_prefix, request, param, user)
	}
	if flag == false {
		param["ERROR"] = "CSRF error"
		return showLoginPageTemplate(url_prefix, request, param, user)
	}
	user, err = tinylib.LoginLocalUser(postData["username"], postData["password"])
	if err != nil {
		param["ERROR"] = fmt.Sprintf("Invalid Account or Password.")
		log.Printf("Invalid Account or Password. %v", err)
		return showLoginPageTemplate(url_prefix, request, param, user)
	}
	session, err := GenerateSession(user)
	if err != nil {
		param["ERROR"] = "Session ERROR"
		return showLoginPageTemplate(url_prefix, request, param, user)
	}
	param = AddCookie2Param(param, "sessions", session, true)
	redir := postData["redirect"]
	if redir == "" {
		redir = "/"
	}
	return Redirect(redir, param), nil
}

func logout(url_prefix string, request events.APIGatewayProxyRequest, param map[string]interface{}, user tinylib.UserInterface) (events.APIGatewayProxyResponse, error) {
	param = AddCookie2Param(param, "sessions", "", true)
	redir := request.QueryStringParameters["redirect"]
	if redir == "" {
		redir = "/"
	}
	return Redirect(redir, param), nil
}

func showLoginPageTemplate(url_prefix string, request events.APIGatewayProxyRequest, param map[string]interface{}, user tinylib.UserInterface) (events.APIGatewayProxyResponse, error) {
	csrf, _ := GenerateCSRF(user, request.Path, 60*5)
	param["CSRF"] = csrf
	if config.GetTwitterConsumerKey() != "" {
		param["TWITTER_LOGIN_URL"] = url_prefix + "/login/twitter/"
	} else {
		param["TWITTER_LOGIN_URL"] = ""
	}
	return ShowTemplate(request, "/tmp/template/account_login.html", param)
}

func account_google_login(url_prefix string, request events.APIGatewayProxyRequest, param map[string]interface{}) (events.APIGatewayProxyResponse, error) {
	param = AddCookie2Param(param, "redirect", param["redirect"].(string), true)
	oauth2g := MakeOauth2Conf4google(config.GetGoogleOauthClient(), config.GetGoogleOauthSecret(), "https://"+request.Headers["Host"]+url_prefix+"/login/google/callback/")
	res, err := oauth2g.Login4google(request, config.GetTinyConfig().GetSessionSalt())
	if err != nil {
		log.Printf("%v", err)
		return ShowTemplate(request, "template/500.html", param)
	}
	return res, err
}

func account_google_login_callback(url_prefix string, request events.APIGatewayProxyRequest, param map[string]interface{}) (events.APIGatewayProxyResponse, error) {
	oauth2g := MakeOauth2Conf4google(config.GetGoogleOauthClient(), config.GetGoogleOauthSecret(), "https://"+request.Headers["Host"]+url_prefix+"/login/google/callback/")
	token, err := oauth2g.LoginCallback4google(request, config.GetTinyConfig().GetSessionSalt())
	if err != nil {
		log.Printf("account_google_login_callback LoginCallback4google:%v", err)
		return ShowTemplate(request, "template/500.html", param)
	}
	user, err := tinylib.GetOrNewUser(token.Email, "google")
	if err != nil {
		log.Printf("account_google_login_callback GetOrNewUser:%v", err)
		return ShowTemplate(request, "template/500.html", param)
	}

	session, err := GenerateSession(user)
	if err != nil {
		param["ERROR"] = "Session ERROR"
		return showLoginPageTemplate(url_prefix, request, param, user)
	}
	param = AddCookie2Param(param, "sessions", session, true)
	redir, err := GetCookieByRequest(request, "redirect")
	if redir != "" {
		param = AddCookie2Param(param, "redirect", "", true)
		return Redirect(redir, param), nil
	}
	return Redirect("/", param), nil
}

func MakeOauth2Conf4google(clientid string, clientsecret string, callback_url string) Oauth2Config {
	conf := Oauth2Config{
		Config: oauth2.Config{
			ClientID:     clientid,
			ClientSecret: clientsecret,
			Scopes:       []string{"openid", "email", "profile"},
			Endpoint: oauth2.Endpoint{
				AuthURL:  "https://accounts.google.com/o/oauth2/v2/auth",
				TokenURL: "https://www.googleapis.com/oauth2/v4/token",
			},
			RedirectURL: callback_url,
		}}
	return conf
}

func (self *Oauth2Config) Login4google(request events.APIGatewayProxyRequest, jwt_secret string) (events.APIGatewayProxyResponse, error) {
	rand.Seed(time.Now().UTC().UnixNano())
	source_str := "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	strlen := 128

	tempR := make([]byte, strlen)
	for i := 0; i < strlen; i++ {
		tempR[i] = source_str[rand.Intn(len(source_str))]
	}
	token := &Oauth2Token{}
	token.Token = string(tempR)
	token.ExpiredDate = time.Now()
	token.ExpiredDate = token.ExpiredDate.Add(time.Duration(1) * time.Hour)

	tokenJwt := jwt.NewWithClaims(jwt.SigningMethodHS512, token)
	tokenStr, err := tokenJwt.SignedString([]byte(jwt_secret))

	res := events.APIGatewayProxyResponse{}
	res.StatusCode = 302
	res.Headers = map[string]string{}
	res.Headers["Set-Cookie"] = "oauth2token=" + tokenStr + "; max-age=3600; Secure"
	res.Headers["Location"] = self.Config.AuthCodeURL(tokenStr)
	res.Body = ""
	res.IsBase64Encoded = false
	return res, err
}

func (self *Oauth2Config) LoginCallback4google(request events.APIGatewayProxyRequest, jwt_secret string) (*v2.Tokeninfo, error) {
	state_url := request.QueryStringParameters["state"]
	rawCookies := request.Headers["Cookie"]
	tempHeader := http.Header{}
	tempHeader.Add("Cookie", rawCookies)
	tempRequest := http.Request{Header: tempHeader}
	tempRequest.Cookies()
	tokenCookie, err := tempRequest.Cookie("oauth2token")

	if err != nil {
		return nil, err
	}
	state_cookie := tokenCookie.Value

	if state_url != state_cookie {
		return nil, errors.New("sservice loginlogout LoginCallback4google:Invalid state")
	}

	token := Oauth2Token{}

	tokenTemp, err := jwt.ParseWithClaims(state_cookie, &token, func(token *jwt.Token) (interface{}, error) {
		return []byte(jwt_secret), nil
	})
	if err != nil || tokenTemp.Valid == false {
		return nil, errors.New("sservice loginlogout LoginCallback4google:Invalid Token1")
	}
	if token.ExpiredDate.Before(time.Now()) {
		return nil, errors.New("sservice loginlogout LoginCallback4google:Timeout")
	}

	code := request.QueryStringParameters["code"]
	token2, err := self.Config.Exchange(oauth2.NoContext, code)
	if err != nil || token2.Valid() == false {
		return nil, errors.New("sservice loginlogout LoginCallback4google:Invalid Token2:" + err.Error())
	}

	service, err := v2.New(self.Config.Client(oauth2.NoContext, token2))
	if err != nil {
		return nil, err
	}

	result, err := service.Tokeninfo().AccessToken(token2.AccessToken).Do()

	return result, err
}

func LoginByTwitter(url_prefix string, request events.APIGatewayProxyRequest, param map[string]interface{}, user tinylib.UserInterface) (events.APIGatewayProxyResponse, error) {
	oc := &oauth.Client{
		TemporaryCredentialRequestURI: twitterRefreshTokenURL,
		ResourceOwnerAuthorizationURI: twitterAuthorizationURL,
		TokenRequestURI:               twitterAccessTokenURL,
		Credentials: oauth.Credentials{
			Token:  config.GetTwitterConsumerKey(),
			Secret: config.GetTwitterConsumerSecret(),
		},
	}
	callbackURL := "https://" + request.Headers["Host"] + url_prefix + "/login/twitter/callback/"
	secure := true

	rt, err := oc.RequestTemporaryCredentials(nil, callbackURL, nil)
	if err != nil {
		log.Printf("LoginByTwitter:Failed to create TemporaryCredentials:%v", err)
		return ShowTemplateWithStatusCode(request, "/tmp/template/500.html", param, 500)
	}
	param = AddCookie2Param(param, "request_token", rt.Token, secure)
	param = AddCookie2Param(param, "request_token_secret", rt.Secret, secure)
	param = AddCookie2Param(param, "redirect", param["redirect"].(string), secure)

	url := oc.AuthorizationURL(rt, nil)
	log.Printf("LoginByTwitter:URL:%v", url)
	return Redirect(url, param), nil
}

func LoginByTwitterCallback(request events.APIGatewayProxyRequest, param map[string]interface{}, user tinylib.UserInterface) (events.APIGatewayProxyResponse, error) {
	log.Printf("LoginByTwitterCallback:%v", request.QueryStringParameters)
	oauth_token := request.QueryStringParameters["oauth_token"]
	oauth_verifier := request.QueryStringParameters["oauth_verifier"]
	request_token, err := GetCookieByRequest(request, "request_token")
	if err != nil {
		log.Printf("LoginByTwitterCallback:Failed to get by cookie:%v", err)
		return ShowTemplateWithStatusCode(request, "/tmp/template/500.html", param, 500)
	}
	request_token_secret, err := GetCookieByRequest(request, "request_token_secret")
	if err != nil {
		log.Printf("LoginByTwitterCallback:Failed to get by cookie:%v", err)
		return ShowTemplateWithStatusCode(request, "/tmp/template/500.html", param, 500)
	}
	if request_token != oauth_token {
		log.Printf("LoginByTwitterCallback:Failed to validate tokens: %v!=%v", request_token, oauth_token)
		return ShowTemplateWithStatusCode(request, "/tmp/template/500.html", param, 500)
	}

	oc := &oauth.Client{
		TemporaryCredentialRequestURI: twitterRefreshTokenURL,
		ResourceOwnerAuthorizationURI: twitterAuthorizationURL,
		TokenRequestURI:               twitterAccessTokenURL,
		Credentials: oauth.Credentials{
			Token:  config.GetTwitterConsumerKey(),
			Secret: config.GetTwitterConsumerSecret(),
		},
	}
	access_token, _, err := oc.RequestToken(nil, &oauth.Credentials{Token: request_token, Secret: request_token_secret}, oauth_verifier)
	if err != nil {
		log.Printf("LoginByTwitterCallback:Failed to get Access Token:%v", err)
		return ShowTemplateWithStatusCode(request, "/tmp/template/500.html", param, 500)
	}
	api := anaconda.NewTwitterApiWithCredentials(access_token.Token, access_token.Secret, config.GetTwitterConsumerKey(), config.GetTwitterConsumerSecret())
	ok, err := api.VerifyCredentials()
	if ok == false || err != nil {
		log.Printf("LoginByTwitterCallback:Failed to validate Access Token:%v,%v,%v", ok, err, access_token)
		return ShowTemplateWithStatusCode(request, "/tmp/template/500.html", param, 500)
	}
	vals := url.Values{}
	vals.Add("include_email", "true")
	aUser, err := api.GetSelf(vals)
	if err != nil {
		log.Printf("LoginByTwitterCallback:Failed to get User info:%v", err)
		return ShowTemplateWithStatusCode(request, "/tmp/template/500.html", param, 500)
	}
	user, err = tinylib.GetOrNewUser(aUser.Email, "twitter")
	if err != nil {
		log.Printf("LoginByTwitterCallback:Failed to get User:%v", err)
		return ShowTemplateWithStatusCode(request, "/tmp/template/500.html", param, 500)
	}
	ttToken, err := user.GetJsonVal("twitter_token")
	if ttToken == "" {
		err = user.SetNickname(aUser.ScreenName)
		if err != nil {
			log.Printf("LoginByTwitterCallback:Failed to SetNickname:%v", err)
			return ShowTemplateWithStatusCode(request, "/tmp/template/500.html", param, 500)
		}
		token, err := tinylib.EncryptByAESwithBase64(access_token.Token)
		if err != nil {
			log.Printf("LoginByTwitterCallback:Failed to Encrypt:%v", err)
			return ShowTemplateWithStatusCode(request, "/tmp/template/500.html", param, 500)
		}
		secret, err := tinylib.EncryptByAESwithBase64(access_token.Secret)
		if err != nil {
			log.Printf("LoginByTwitterCallback:Failed to Encrypt:%v", err)
			return ShowTemplateWithStatusCode(request, "/tmp/template/500.html", param, 500)
		}
		err = user.SetJsonVal("twitter_token", token)
		if err != nil {
			log.Printf("LoginByTwitterCallback:Failed to SetJsonVal:%v", err)
			return ShowTemplateWithStatusCode(request, "/tmp/template/500.html", param, 500)
		}
		err = user.SetJsonVal("twitter_secret", secret)
		if err != nil {
			log.Printf("LoginByTwitterCallback:Failed to SetJsonVal:%v", err)
			return ShowTemplateWithStatusCode(request, "/tmp/template/500.html", param, 500)
		}
	}

	session, err := GenerateSession(user)
	if err == nil {
		param = AddCookie2Param(param, "sessions", session, true)
	}

	redir, err := GetCookieByRequest(request, "redirect")
	if redir != "" {
		secure := request.Headers["X-Forwarded-Proto"] == "https"
		param = AddCookie2Param(param, "redirect", "", secure)
		return Redirect(redir, param), nil
	}
	return Redirect("/", param), nil
}
