package pkg

import (
	"fmt"
	"strings"

	"bitbucket.org/ccat/tinylib"
	"github.com/aws/aws-lambda-go/events"
)

func ShowConfig(request events.APIGatewayProxyRequest, param map[string]interface{}, user tinylib.UserInterface) (events.APIGatewayProxyResponse, error) {
	mode := request.QueryStringParameters["mode"]
	param["mode"] = mode
	param["Status"] = config.GetStatus()
	csrf, _ := GenerateCSRF(user, request.Path, 60*5)
	param["CSRF"] = csrf

	param["requestText"] = fmt.Sprintf("%v", request)
	if config.GetStatusCode() <= 10 {
		return showConfigInitial(request, param, user)
	}
	if user == nil || user.IsAdmin() == false {
		return Redirect(config.GetURL_PREFIX()+"/account/login/?redirect="+config.GetURL_PREFIX()+"/config/", param), nil
	}
	if strings.HasPrefix(request.Path, config.GetURL_PREFIX()+"/config/user/") {
		return showConfigUser(request, param, user)
	}
	if strings.HasPrefix(request.Path, config.GetURL_PREFIX()+"/config/media/") {
		//return showMediaLibrary(request, param, user)
	}
	return ShowTemplate(request, "/tmp/template/config.html", param)
}

func showConfigInitial(request events.APIGatewayProxyRequest, param map[string]interface{}, user tinylib.UserInterface) (events.APIGatewayProxyResponse, error) {
	mode := request.QueryStringParameters["mode"]
	if mode == "createroot" {
		if request.HTTPMethod == "GET" {
			return ShowTemplate(request, "/tmp/template/config_createRootUser.html", param)
		} else if request.HTTPMethod == "POST" {
			postData, err := ParseFormUrlencoded(request)
			if err != nil {
				param["ERROR"] = err
				return ShowTemplate(request, "/tmp/template/config_createRootUser.html", param)
			}
			flag, err := ValidateCSRF(postData["CSRF"], user, request.Path)
			if err != nil {
				param["ERROR"] = err
				return ShowTemplate(request, "/tmp/template/config_createRootUser.html", param)
			}
			if flag == false {
				param["ERROR"] = "CSRF error"
				return ShowTemplate(request, "/tmp/template/config_createRootUser.html", param)
			}
			err = tinylib.CreateLocalRootUser(postData["username"], postData["password"])
			if err != nil {
				param["ERROR"] = err
				return ShowTemplate(request, "/tmp/template/config_createRootUser.html", param)
			}
			config.UpdateStatus("root_created")
			config.UpdateStatusCode(11)
			err = config.Save()
			if err != nil {
				param["ERROR"] = err
				return ShowTemplate(request, "/tmp/template/config_createRootUser.html", param)
			}
			return Redirect(config.GetURL_PREFIX()+"/config/", param), nil
		}
	}
	return ShowTemplate(request, "/tmp/template/config.html", param)
}

func showConfigUser(request events.APIGatewayProxyRequest, param map[string]interface{}, user tinylib.UserInterface) (events.APIGatewayProxyResponse, error) {
	if request.Path == config.GetURL_PREFIX()+"/config/user/" {
		return showConfigUserList(request, param, user)
	} else {
		return showConfigUserSingle(request, param, user)
	}
	return Redirect(config.GetURL_PREFIX()+"/config/", param), nil
}

func showConfigUserList(request events.APIGatewayProxyRequest, param map[string]interface{}, user tinylib.UserInterface) (events.APIGatewayProxyResponse, error) {
	key := request.QueryStringParameters["key"]
	uList, pKey, err := tinylib.GetAllUser(key)
	param["ERROR"] = err
	param["pageKey"] = pKey
	param["users"] = uList
	return ShowTemplate(request, "/tmp/template/config_userlist.html", param)
}

func showConfigUserSingle(request events.APIGatewayProxyRequest, param map[string]interface{}, user tinylib.UserInterface) (events.APIGatewayProxyResponse, error) {
	path := request.Path
	if strings.HasSuffix(path, "/") {
		path = path[:len(path)-1]
	}
	pathList := strings.Split(path, "/")
	lastPath := pathList[len(pathList)-1]
	tUser, err := tinylib.GetUser(lastPath)
	param["ERROR"] = err
	param["target"] = tUser
	if err != nil {
		return ShowTemplate(request, "/tmp/template/config_user.html", param)
	}

	if request.HTTPMethod == "POST" {
		postData, err := ParseFormUrlencoded(request)
		if err != nil {
			param["ERROR"] = err
			return ShowTemplate(request, "/tmp/template/config_user.html", param)
		}
		flag, err := ValidateCSRF(postData["CSRF"], user, request.Path)
		if err != nil {
			param["ERROR"] = err
			return ShowTemplate(request, "/tmp/template/config_user.html", param)
		}
		if flag == false {
			param["ERROR"] = "CSRF error"
			return ShowTemplate(request, "/tmp/template/config_user.html", param)
		}
		err = tUser.SetBasicAttr(postData["Disabled"] == "true", postData["Admin"] == "true", postData["Staff"] == "true")
		param["ERROR"] = err
	}

	return ShowTemplate(request, "/tmp/template/config_user.html", param)
}
