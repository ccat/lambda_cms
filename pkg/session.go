package pkg

import (
	//"fmt"
	"net/http"
	"time"

	"bitbucket.org/ccat/tinylib"
	"github.com/aws/aws-lambda-go/events"
	//jwt "github.com/dgrijalva/jwt-go"
	//"github.com/guregu/dynamo"
	"github.com/o1egl/paseto"
	"github.com/pkg/errors"
	//"golang.org/x/crypto/bcrypt"
)

/*type UserSession struct {
	UserSort    string
	ExpiredDate time.Time
	//jwt.StandardClaims
}*/

/*type UserSession struct {
	Prime       string `dynamo:"prime"`
	Sort        string `dynamo:"sort"`
	IndexId     int64
	SecondIndex string //User
	UserSort    string //User
	OtherData   map[string]string
	TTL         int64
}*/

var (
	ErrorInvalidToken   = errors.New("Invalid Token.")
	ErrorSessionExpired = errors.New("Session Expired.")
)

func GetCookieByRequest(request events.APIGatewayProxyRequest, cookie_name string) (string, error) {
	rawCookies := request.Headers["Cookie"]
	tempHeader := http.Header{}
	tempHeader.Add("Cookie", rawCookies)
	tempRequest := http.Request{Header: tempHeader}
	tempRequest.Cookies()
	tokenCookie, err := tempRequest.Cookie(cookie_name)
	if err != nil {
		return "", err
	}
	return tokenCookie.Value, nil
}

func GetUserByRequest(request events.APIGatewayProxyRequest) (tinylib.UserInterface, error) {
	cookieVal, err := GetCookieByRequest(request, "sessions")
	if err != nil {
		if err == http.ErrNoCookie {
			return tinylib.GetAnonymousUser(), nil
		}
		return nil, err
	}
	return GetUserBySession(cookieVal)
}

func GetUserBySession(tokenStr string) (tinylib.UserInterface, error) {
	/*token := UserSession{}

	tokenTemp, err := jwt.ParseWithClaims(jwtStr, &token, func(token *jwt.Token) (interface{}, error) {
		return []byte(config.GetTinyConfig().GetSessionSalt()), nil
	})
	if err != nil || tokenTemp.Valid == false {
		return nil, ErrorInvalidToken
	}
	if token.ExpiredDate.Before(time.Now()) {
		return nil, ErrorSessionExpired
	}*/
	v2 := paseto.NewV2()
	tokenStr = "v2.local." + tokenStr

	var newJsonToken paseto.JSONToken
	var newFooter string
	err := v2.Decrypt(tokenStr, []byte(config.GetTinyConfig().GetSessionSalt()), &newJsonToken, &newFooter)
	if err != nil {
		return nil, errors.Wrap(err, "GetUserBySession")
	}
	if newJsonToken.Expiration.Before(time.Now()) {
		return nil, ErrorSessionExpired
	}

	user, err := tinylib.GetUser(newJsonToken.Audience)
	if err != nil {
		return nil, errors.Wrap(err, "GetUserBySession")
	}
	return user, nil
}

func GenerateSession(user tinylib.UserInterface) (string, error) {
	/*token := &UserSession{}
	token.UserSort = user.GetSort()
	token.ExpiredDate = time.Now()
	token.ExpiredDate = token.ExpiredDate.Add(time.Duration(7*24) * time.Hour)

	tokenJwt := jwt.NewWithClaims(jwt.SigningMethodHS512, token)
	return tokenJwt.SignedString([]byte(config.GetTinyConfig().GetSessionSalt()))*/

	now := time.Now()
	exp := now.Add(7 * 24 * time.Hour)
	nbt := now

	jsonToken := paseto.JSONToken{
		Audience:   user.GetSort(),
		IssuedAt:   now,
		Expiration: exp,
		NotBefore:  nbt,
	}
	footer := "no footer"

	v2 := paseto.NewV2()
	token, err := v2.Encrypt([]byte(config.GetTinyConfig().GetSessionSalt()), jsonToken, footer)
	if err != nil {
		return "", err
	}
	return token[9:], nil
}
