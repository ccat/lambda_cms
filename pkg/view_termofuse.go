package pkg

import (
	"bitbucket.org/ccat/tinylib"
	"github.com/aws/aws-lambda-go/events"
)

func ShowTermOfUsePage(url_prefix string, request events.APIGatewayProxyRequest, param map[string]interface{}, user tinylib.UserInterface) (events.APIGatewayProxyResponse, error) {
	if request.Path == url_prefix+"/edit/" {
		return showTermOfUseEditPage(url_prefix, request, param, user)
	}
	term := config.GetTermOfUse()
	param["TermOfUse"] = term.GetHTML()
	csrf, _ := GenerateCSRF(user, request.Path, 60*5)
	param["CSRF"] = csrf
	tempS, _ := user.GetJsonVal("term_of_use_accept_date")
	param["accepted"] = tempS == config.GetTermOfUse().GetPublishDate()

	if request.HTTPMethod == "POST" {
		postData, err := ParseFormUrlencoded(request)
		if err != nil {
			return ShowTemplate(request, "/tmp/template/termofuse.html", param)
		}
		flag, err := ValidateCSRF(postData["CSRF"], user, request.Path)
		if err != nil {
			return ShowTemplate(request, "/tmp/template/termofuse.html", param)
		}
		if flag == false {
			return ShowTemplate(request, "/tmp/template/termofuse.html", param)
		}
		if postData["accept"] == "yes" {
			user.SetJsonVal("term_of_use_accept_date", config.GetTermOfUse().GetPublishDate())
			red := request.QueryStringParameters["redirect"]
			if red != "" {
				return Redirect(red, param), nil
			}
			tempS, _ = user.GetJsonVal("term_of_use_accept_date")
			param["accepted"] = tempS == config.GetTermOfUse().GetPublishDate()
		}
	}
	return ShowTemplate(request, "/tmp/template/termofuse.html", param)
}

func showTermOfUseEditPage(url_prefix string, request events.APIGatewayProxyRequest, param map[string]interface{}, user tinylib.UserInterface) (events.APIGatewayProxyResponse, error) {
	if user == nil || user.IsAdmin() == false {
		return Redirect(config.GetURL_PREFIX()+"/account/login/?redirect="+url_prefix+"/edit/", param), nil
	}
	csrf, _ := GenerateCSRF(user, request.Path, 60*5)
	param["CSRF"] = csrf
	term, _ := GetDraftTermOfUse()
	param["TermOfUse"] = term.GetHTML()
	if request.HTTPMethod == "GET" {
		return ShowTemplate(request, "/tmp/template/termofuse_edit.html", param)
	} else if request.HTTPMethod == "POST" {
		postData, err := ParseFormUrlencoded(request)
		if err != nil {
			param["ERROR"] = err
			return ShowTemplate(request, "/tmp/template/termofuse_edit.html", param)
		}
		flag, err := ValidateCSRF(postData["CSRF"], user, request.Path)
		if err != nil {
			param["ERROR"] = err
			return ShowTemplate(request, "/tmp/template/termofuse_edit.html", param)
		}
		if flag == false {
			param["ERROR"] = "CSRF error"
			return ShowTemplate(request, "/tmp/template/termofuse_edit.html", param)
		}
		term.SetHTML(postData["draft"])
		err = term.Save()
		if err != nil {
			param["ERROR"] = err
			return ShowTemplate(request, "/tmp/template/termofuse_edit.html", param)
		}
		if postData["pubulish"] == "yes" {
			err = term.Publish()
			if err != nil {
				param["ERROR"] = err
				return ShowTemplate(request, "/tmp/template/termofuse_edit.html", param)
			}
			config.setTermOfUse(term)
		}
	}
	return Redirect(url_prefix+"/", param), nil
}
