package pkg

import (
	"os"
	"testing"
	"time"

	"bitbucket.org/ccat/tinylib"
	jwt "github.com/dgrijalva/jwt-go"
)

func TestSession(t *testing.T) {
	config = nil
	err := os.Setenv("AWS_REGION", "us-east-1")
	if err != nil {
		t.Fatalf("failed os.Setenv %#v", err)
	}
	err = InitCmsConfig()
	if err != nil {
		t.Fatalf("failed InitCmsConfig %#v", err)
	}
	u2, err := tinylib.CreateLocalUser("testuser", "password", "nickname")
	if err != nil {
		t.Fatalf("failed CreateLocalUser %#v", err)
	}
	u1 := u2
	sessionStr, err := GenerateSession(u2)
	if err != nil {
		t.Fatalf("failed GenerateSession %#v", err)
	}
	u2, err = GetUserBySession(sessionStr)
	if err != nil {
		t.Fatalf("failed GetUserBySession %#v", err)
	}
	u2, err = GetUserBySession(sessionStr + "aaa")
	if err != ErrorInvalidToken {
		t.Fatalf("failed GetUserBySession %#v,%#v", err, u2)
	}
	token := &UserSession{}
	token.UserSort = u1.GetSort()
	token.ExpiredDate = time.Now()
	token.ExpiredDate = token.ExpiredDate.Add(-time.Duration(7*24) * time.Hour)
	tokenJwt := jwt.NewWithClaims(jwt.SigningMethodHS512, token)
	sessionStr, err = tokenJwt.SignedString([]byte(config.GetTinyConfig().GetSessionSalt()))
	if err != nil {
		t.Fatalf("failed Generate Test Session %#v", err)
	}
	u2, err = GetUserBySession(sessionStr)
	if err != ErrorSessionExpired {
		t.Fatalf("failed GetUserBySession %#v,%#v", err, u2)
	}
}
