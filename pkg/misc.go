package pkg

import (
	"net/url"
	"strings"
	"time"

	"bitbucket.org/ccat/tinylib"
	"github.com/aws/aws-lambda-go/events"
	//jwt "github.com/dgrijalva/jwt-go"
	"github.com/o1egl/paseto"
	"github.com/pkg/errors"
)

/*type CSRFtoken struct {
	UserSort    string
	URL         string
	ExpiredDate time.Time
	jwt.StandardClaims
}*/

var (
	NoFormUrlencoded     = errors.New("Request's content-type is not application/x-www-form-urlencoded ")
	NoFormPost           = errors.New("Request is not POST")
	ErrorCSRFExpired     = errors.New("CSRF Expired.")
	ErrorCSRFInvalidUser = errors.New("CSRF Invalid User.")
	ErrorCSRFInvalidURL  = errors.New("CSRF Invalid URL.")
)

func GenerateCSRF(user tinylib.UserInterface, url string, seconds int) (string, error) {
	/*token := &CSRFtoken{}
	token.UserSort = user.GetSort()
	token.URL = url
	token.ExpiredDate = time.Now()
	token.ExpiredDate = token.ExpiredDate.Add(time.Duration(seconds) * time.Second)

	tokenJwt := jwt.NewWithClaims(jwt.SigningMethodHS512, token)
	return tokenJwt.SignedString([]byte(config.GetTinyConfig().GetSessionSalt()))*/

	now := time.Now()
	exp := now.Add(time.Duration(seconds) * time.Second)
	nbt := now

	jsonToken := paseto.JSONToken{
		Audience:   user.GetSort(),
		IssuedAt:   now,
		Expiration: exp,
		NotBefore:  nbt,
	}
	jsonToken.Set("url", url)
	footer := "no footer"

	v2 := paseto.NewV2()
	token, err := v2.Encrypt([]byte(config.GetTinyConfig().GetSessionSalt()), jsonToken, footer)
	if err != nil {
		return "", err
	}
	return token[9:], nil

}

func ValidateCSRF(csrf string, user tinylib.UserInterface, url string) (bool, error) {
	/*token := CSRFtoken{}

	tokenTemp, err := jwt.ParseWithClaims(csrf, &token, func(token *jwt.Token) (interface{}, error) {
		return []byte(config.GetTinyConfig().GetSessionSalt()), nil
	})
	if err != nil || tokenTemp.Valid == false {
		return false, ErrorInvalidToken
	}
	if token.ExpiredDate.Before(time.Now()) {
		return false, ErrorCSRFExpired
	}
	if token.UserSort != user.GetSort() {
		return false, ErrorCSRFInvalidUser
	}
	if token.URL != url {
		return false, ErrorCSRFInvalidURL
	}
	return true, nil*/

	v2 := paseto.NewV2()
	csrf = "v2.local." + csrf

	var newJsonToken paseto.JSONToken
	var newFooter string
	err := v2.Decrypt(csrf, []byte(config.GetTinyConfig().GetSessionSalt()), &newJsonToken, &newFooter)
	if err != nil {
		return false, errors.Wrap(err, "ValidateCSRF")
	}
	if newJsonToken.Expiration.Before(time.Now()) {
		return false, ErrorCSRFExpired
	}

	if newJsonToken.Audience != user.GetSort() {
		return false, ErrorCSRFInvalidUser
	}
	if newJsonToken.Get("url") != url {
		return false, ErrorCSRFInvalidURL
	}
	return true, nil
}

func ParseFormUrlencoded(request events.APIGatewayProxyRequest) (map[string]string, error) {
	result := map[string]string{}
	if request.HTTPMethod != "POST" {
		return result, NoFormPost
	}
	val, ok := request.Headers["content-type"]
	if ok == false || val != "application/x-www-form-urlencoded" {
		return result, NoFormUrlencoded
	}
	parts := strings.Split(request.Body, "&")
	for i := 0; i < len(parts); i++ {
		tempP := strings.Split(parts[i], "=")
		if len(tempP) == 2 {
			data, err := url.QueryUnescape(tempP[1])
			if err == nil {
				result[tempP[0]] = data
			}
		}
	}
	return result, nil
}

func AddCookie2Param(param map[string]interface{}, key string, val string, secure bool) map[string]interface{} {
	headers := map[string]string{}
	temp, ok := param["HEADERS"]
	if ok == true {
		headers, _ = temp.(map[string]string)
	}
	tempStr := key + "=" + val + "; max-age=31536000;"
	cval, ok := param["COOKIE_DOMAIN"]
	if ok {
		tempStr = tempStr + " domain=" + cval.(string) + "; "
	}
	cval, ok = param["COOKIE_PATH"]
	if ok {
		tempStr = tempStr + " path=" + cval.(string) + "; "
	}
	if secure == true {
		tempStr += "Secure"
	}
	_, ok = headers["Set-Cookie"]
	if ok == false {
		headers["Set-Cookie"] = tempStr
	} else {
		_, ok = headers["Set-cookie"]
		if ok == false {
			headers["Set-cookie"] = tempStr
		} else {
			_, ok = headers["set-Cookie"]
			if ok == false {
				headers["set-Cookie"] = tempStr
			} else {
				headers["set-cookie"] = tempStr
			}
		}
	}
	param["HEADERS"] = headers
	return param
}

func GenerateParam(request events.APIGatewayProxyRequest) (map[string]interface{}, tinylib.UserInterface) {
	if config == nil {
		InitCmsConfig()
	}
	param := map[string]interface{}{}
	param["path"] = request.Path
	param["AWS_REGION"] = config.GetTinyConfig().GetAWSregion() //us-east-1, ap-northeast-1,...
	param["DynamoDB_TABLE"] = config.GetTinyConfig().GetDynamoDBtableName()
	//param["DynamoDB_TABLE_PREFIX"] = config.GetTinyConfig().GetDynamoDBprefix()
	user, err := GetUserByRequest(request)
	if err == nil {
		param["user"] = user
	} else {
		user = tinylib.GetAnonymousUser()
		param["user"] = user
	}
	return param, user
}
