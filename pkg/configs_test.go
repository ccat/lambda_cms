package pkg

import (
	"os"
	"testing"
)

func TestInitCmsConfig(t *testing.T) {
	err := os.Setenv("AWS_REGION", "us-east-1")
	if err != nil {
		t.Fatalf("failed os.Setenv %#v", err)
	}
	err = InitCmsConfig()
	if err != nil {
		t.Fatalf("failed InitCmsConfig %#v", err)
	}
}
