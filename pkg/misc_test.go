package pkg

import (
	"os"
	"testing"

	"bitbucket.org/ccat/tinylib"
)

func TestMisc(t *testing.T) {
	config = nil
	err := os.Setenv("AWS_REGION", "us-east-1")
	if err != nil {
		t.Fatalf("failed os.Setenv %#v", err)
	}
	err = InitCmsConfig()
	if err != nil {
		t.Fatalf("failed InitCmsConfig %#v", err)
	}
	u2, err := tinylib.CreateLocalUser("testuserMisc", "password", "nickname")
	if err != nil {
		t.Fatalf("failed CreateLocalUser %#v", err)
	}
	csrfStr, err := GenerateCSRF(u2, "/", 200)
	if err != nil {
		t.Fatalf("failed GenerateCSRF %#v", err)
	}
	csrfCheck, err := ValidateCSRF(csrfStr, u2, "/")
	if err != nil {
		t.Fatalf("failed ValidateCSRF %#v", err)
	}
	if csrfCheck != true {
		t.Fatalf("failed ValidateCSRF False")
	}
	csrfStr, err = GenerateCSRF(u2, "/", -1)
	if err != nil {
		t.Fatalf("failed GenerateCSRF %#v", err)
	}
	csrfCheck, err = ValidateCSRF(csrfStr, u2, "/")
	if err != ErrorCSRFExpired {
		t.Fatalf("failed ValidateCSRF %#v", err)
	}
	if csrfCheck != false {
		t.Fatalf("failed ValidateCSRF True")
	}
}
