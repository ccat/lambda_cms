package pkg

import (
	"time"

	"github.com/guregu/dynamo"
)

type TermOfUseInterface interface {
	Save() error
	Publish() error
	GetHTML() string
	SetHTML(string)
	GetPublishDate() string
}

type termOfUse1 struct {
	Prime       string `dynamo:"prime"` //"TERMOFUSE"
	Sort        string `dynamo:"sort"`  //LATEST, DRAFT or publish date
	Html        string
	PublishDate string
}

func (self *termOfUse1) GetHTML() string {
	return self.Html
}

func (self *termOfUse1) SetHTML(term string) {
	self.Html = term
}

func (self *termOfUse1) Save() error {
	return config.GetTinyConfig().GetDynamoDBtable().Put(self).Run()
}

func (self *termOfUse1) Publish() error {
	self.Sort = time.Now().Format("2006/01/02 15:04:05")
	self.PublishDate = self.Sort
	err := self.Save()
	if err != nil {
		return err
	}
	self.Sort = "LATEST"
	return self.Save()
}

func (self *termOfUse1) GetPublishDate() string {
	return self.PublishDate
}

func GetLatestTermOfUse() (TermOfUseInterface, error) {
	//table := config.GetTinyConfig().GetDynamoDBtable()
	result2 := []termOfUse1{}
	//err := table.Get("prime", config.GetTinyConfig().GetDynamoDBprefix()+"_TERMOFUSE").Range("sort", dynamo.Equal, "LATEST").All(&result2)
	err := config.GetTinyConfig().DynamoTableGet("TERMOFUSE").Range("sort", dynamo.Equal, "LATEST").All(&result2)
	if err != nil || len(result2) == 0 {
		return &termOfUse1{Html: "Not Found.", Prime: config.GetTinyConfig().GenerateDynamoDBPrime("TERMOFUSE"), Sort: "LATEST"}, err
	}
	return &result2[0], nil
}

func GetDraftTermOfUse() (TermOfUseInterface, error) {
	//table := config.GetTinyConfig().GetDynamoDBtable()

	result2 := []termOfUse1{}
	//err := table.Get("prime", config.GetTinyConfig().GetDynamoDBprefix()+"_TERMOFUSE").Range("sort", dynamo.Equal, "DRAFT").All(&result2)
	err := config.GetTinyConfig().DynamoTableGet("TERMOFUSE").Range("sort", dynamo.Equal, "DRAFT").All(&result2)
	if err != nil || len(result2) == 0 {
		draft := termOfUse1{Html: config.GetTermOfUse().GetHTML(), Prime: config.GetTinyConfig().GenerateDynamoDBPrime("TERMOFUSE"), Sort: "DRAFT"}
		return &draft, err
	}
	return &result2[0], nil
}
